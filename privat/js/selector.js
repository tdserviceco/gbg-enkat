jQuery(function($) {
    var answers = {

        '1.Varför valde du att bo här?': ['A.Kärleken','B.Arbetet','C.Född här','D.Släkt/vänner','E.Miljön','F.Boendet'],
        '2.Hur tycker du det är att bo i Lundby stadsdel?': ['A.Mycket bra','B.Ganska bra','C.Varken bra eller dåligt','D.Ganska dåligt','E.Mycket dåligt'],
        '3.Vad tycker du är bra med att bo i Lundby stadsdel?': ['A.Centralt (Nära stan)','B.Grönt','C.Lugnt','D.Nära havet (vatten)','E.Bra kommunikationer','F.Nära köpcentra','G.Nära till allt'],
        '4.Finns det något du saknar i Lundby stadsdel?': ['A.Resturanger','B.Butiker','C.Nöjesliv/kultur','D.Grönområden','E.Kommuniaktioner','F.Belysning','G.Cykelbanor','H.Service (post,vård,barnomsorg etc)'],
        '5.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?': ['A.Centralt (Nära stan)','B.Grönt','C.Lugnt','D.Marint','E.Trafikerat','F.Shopping/servicemöljigheter'],
        '6.Känner du dig trygg i Lundby stadsdel?': ['Ja','--NEJ--','A.Brottslighet','B.Bråk','C.Folktomt (ödsligt)','D.Få poliser','E.Mörkt/dåligt upplyst','F.Inbrott'],
        '7.Tycker du att Lundby stadel har något speciell styrka?': ['Nej','--JA--','A.Centralt (nära stan)','B.Här händer mycket (utveckling,boende,jobb)','C.Grönt','D.Lugnt','E.Bra bostadspriser','F.Bra föreningsmöljigheter'],
        '8.Tycker du att Lundby stadsdel har någon speciell svaghet?': ['Nej','--JA--','A.Alla vägar/leder och trafik','B.Brottslighet/otryggt','C.Nöjesliv/kultur','D.Dåliga förbindelser till Gbg centrum'],
        '9.Hur skulle du vilja att Lundby uppfattades om 5år?': ['Den gröna stadsdelen i Göteborg','Den rockiga stadsdelen i Göteborg','Den lyxiga stadsdelen i Göteborg','Hippa stadsdelen i Göteborg','Den lantiga stadsdelen i Göteborg','Den lugna stadsdelen i Göteborg','Den alternativa stadsdelen i Göteborg','Den mångkulturella stadsdelen i Göteborg','Den framtida stadsdelen i Göteborg'],
        '10.Vilket område förknippad du med stadsdelen Lundby?': ['A.Backaplan','B.Wieselgrenplatsen','C.Kvilletorget','D.Norra Älvstranden','E.Lundbybadet/Rambergsvallen','F.Kärradalen','G.Kyrkbyn'],
        '11.Var i Lundby bor du?': ['A.Kvillebäcken','B.Slättadamm','C.Kärrdalen','D.Kyrkbyn','E.Rambergstaden','F.Eriksberg','G.Lindholmen'],
    
    }
    
    var $answers = $('#answer');
    $('#question').change(function () {
        var question = $(this).val(), lcns = answers[question] || [];
        
        var html = $.map(lcns, function(lcn){
            return '<option value="' + lcn + '" name="answer">' + lcn + '</option>'
        }).join('');
        $answers.html(html)
    });
});
