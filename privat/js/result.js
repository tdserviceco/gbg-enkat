$(document).ready(function() {
	counts();
});
	
function counts(){
	$("#result").submit(function() {
		var data = $("#result").serialize();
		$.post('include/getResult.php',data,function(data)
		{
			$("ul").empty();
			data = JSON.parse(data);
			$.each(data.result, function(){
			// % from the collection
			var number1 = this['countAnswer'];
			var total1 = 500;
			var result1 = (number1 / total1) * 100;
			//-------
			var number2 = this['countMale'];
			var total2 = 500;
			var result2 = (number2 / total2) * 100;
			//------- 
			var number3 = this['countFemale'];
			var total3 = 500;
			var result3 = (number1 / total1) * 100;
			//-------
			var number4 = this['countAge10To20'];
			var total4 = 500;
			var result4 = (number4 / total4) * 100;
			//-------
			var number5 = this['countAge21To30'];
			var total5 = 500;
			var result5 = (number5 / total5) * 100;
			//-------
			var number6 = this['countAge31To40'];
			var total6 = 500;
			var result6 = (number5 / total5) * 100;
			//-------
			var number7 = this['countAge41To50'];
			var total7 = 500;
			var result7 = (number5 / total5) * 100; 
			//-------
			var number8 = this['countAge51To65'];
			var total8 = 500;
			var result8 = (number5 / total5) * 100; 
			//-------
			var number9 = this['countAge66Plus'];
			var total9 = 500;
			var result9 = (number5 / total5) * 100;  
			$("ul").append("<li class='axis'><div class='label'>500 st</div><div class='label'>250 st</div><div class='label'>150 st</div><div class='label'>100 st</div><div class='label'>50 st</div></li><li class='bar teal' style='height:"+result1+"%;'><div class='percent'>"+this['countAnswer']+"<span>st</span></div><div class='skill'>Antal Svar</div></li><li class='bar salmon' style='height:"+result2+"%;'><div class='percent'>"+this['countMale']+"<span>st</span></div><div class='skill'>Män</div></li><li class='bar lime' style='height:"+result3+"%;'><div class='percent'>"+this['countFemale']+"<span>st</span></div><div class='skill'>Kvinnor</div></li><li class='bar peach' style='height:"+result4+"%;'><div class='percent'>"+this['countAge10To20']+"<span>st</span></div><div class='skill'>10-20år</div></li><li class='bar peach' style='height:"+result5+"%;'><div class='percent'>"+this['countAge21To30']+"<span>st</span></div><div class='skill'>21-30år</div></li><li class='bar peach' style='height:"+result6+"%;'><div class='percent'>"+this['countAge31To40']+"<span>st</span></div><div class='skill'>31-40år</div></li><li class='bar peach' style='height:"+result7+"%;'><div class='percent'>"+this['countAge41To50']+"<span>st</span></div><div class='skill'>41-50år</div></li><li class='bar peach' style='height:"+result8+"%;'><div class='percent'>"+this['countAge51To65']+"<span>st</span></div><div class='skill'>51-65år</div></li><li class='bar peach' style='height:"+result9+"%;'><div class='percent'>"+this['countAge66Plus']+"<span>st</span></div><div class='skill'>66+år</div></li>");
		});
		});

		return false;
	});
}
