<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header>&nbsp;</header>
        <div id="bodyContainer">
        <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
        <form id="result">
            <select id="question" name="question">
                <option></option>
                <option value="1.Varför valde du att bo här?" name="question">1.Varför valde du att bo här?
                </option>
                <option value="2.Hur tycker du det är att bo i Lundby stadsdel?" name="question">2.Hur tycker du det är att bo i Lundby stadsdel?</option>
                <option value="3.Vad tycker du är bra med att bo i Lundby stadsdel?" name="question">3.Vad tycker du är bra med att bo i Lundby stadsdel?</option>
                <option value="4.Finns det något du saknar i Lundby stadsdel?" name="question">4.Finns det något du saknar i Lundby stadsdel?</option>
                <option value="5.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?" name="question">5.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?</option>
                <option value="6.Känner du dig trygg i Lundby stadsdel?" name="question">6.Känner du dig trygg i Lundby stadsdel?</option>
                <option value="7.Tycker du att Lundby stadel har något speciell styrka?" name="question">7.Tycker du att Lundby stadel har något speciell styrka?</option>
                <option value="8.Tycker du att Lundby stadsdel har någon speciell svaghet?" name="question">8.Tycker du att Lundby stadsdel har någon speciell svaghet?</option>
                <option value="9.Hur skulle du vilja att Lundby uppfattades om 5år?" name="question">9.Hur skulle du vilja att Lundby uppfattades om 5år?</option>
                <option value="10.Vilket område förknippad du med stadsdelen Lundby?" name="question">10.Vilket område förknippad du med stadsdelen Lundby?</option>
                <option value="11.Var i Lundby bor du?" name="question">11.Var i Lundby bor du?</option>
            </select>
            <br />
            <br />
            <select id="answer" name="answer"></select>
            <input type="submit" name="submit" id="submit" value="Skicka">
        </form>
        <br />
    </div>
    <ul class="chart">
    </ul>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/result.js"></script>
    <script type="text/javascript" src="js/selector.js"></script>
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    </body>
</html>
