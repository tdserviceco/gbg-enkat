<?php

include_once 'mysqli.php';

$question = $_POST['question'];
$answer = $_POST['answer'];

$res = mysqli_query($link,"SELECT 
	COUNT(answer.answers) AS answers,
	COUNT(CASE WHEN gender = 'male' THEN users.uid END) as males, 
	COUNT(CASE WHEN gender = 'female' THEN users.uid END) as females, 
	COUNT(CASE WHEN age = '10-20' THEN users.age END) as age10To20,
	COUNT(CASE WHEN age = '21-30' THEN users.age END) as age21To30,
	COUNT(CASE WHEN age = '31-40' THEN users.age END) as age31To41, 
	COUNT(CASE WHEN age = '41-50' THEN users.age END) as age41To50,
	COUNT(CASE WHEN age = '51-65' THEN users.age END) as age51To65,
	COUNT(CASE WHEN age = '66+' THEN users.age END) as age66Plus
FROM
	 users 

INNER JOIN 
	answer ON answer.uid = users.uid 

INNER JOIN 
	question ON question.uid = users.uid 

WHERE 
	question.questions = '$question' AND answer.answers = '$answer'");
$result = array();
while($row = mysqli_fetch_array($res))
  {
  	array_push($result, array("countAnswer" => $row[0],
  							  "countMale" => $row[1],
  							  "countFemale" => $row[2],
  							  "countAge10To20" => $row[3],
  							  "countAge21To30" => $row[4],
  							  "countAge31To40" => $row[5],
  							  "countAge41To50" => $row[6],
  							  "countAge51To65" => $row[7],
  							  "countAge66Plus" => $row[8]
  							  ));
  }
echo json_encode(array("result"=>$result));
mysqli_close($link);
