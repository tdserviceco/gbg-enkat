<!DOCTYPE html>
<!--
THIS SITE IS OPTIMISED FOR TABLETS ONLY!!! MAINLY IPADS!
THE CLIENT WANTED IT USED FOR IPAD MINI OR IPAD 2/3/4/AIR
-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header><div id="linkToResult"><a href="result.php" target="_self">Resultat sidan</a></header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
    <div id="questions">
      <form id="privateForm">
      <ul>
      <li>
        <label>1.Varför valde du att bo här?</label>
        <input type="text" value="1.Varför valde du att bo här?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Kärleken" name="answer[]"><label>A.Kärleken</label></li>
        <li><input type="checkbox" value="B.Arbetet" name="answer[]"><label>B.Arbetet</label></li>
        <li><input type="checkbox" value="C.Född här" name="answer[]"><label>C.Född här</label></li>
        <li><input type="checkbox" value="D.Släkt/vänner" name="answer[]"><label>D.Släkt/vänner</label></li>
        <li><input type="checkbox" value="E.Miljön" name="answer[]"><label>E.Miljön</label></li>
        <li><input type="checkbox" value="F.Boendet" name="answer[]"><label>F.Boendet</label></li>
      </ul>
      <ul>
        <li><label>2.Hur tycker du det är att bo i Lundby stadsdel?</label>
        <input type="text" value="2.Hur tycker du det är att bo i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Mycket bra" name="answer[]"><label>A.Mycket bra</label></li>
        <li><input type="checkbox" value="B.Ganska bra" name="answer[]"><label>B.Ganska bra</label></li>
        <li><input type="checkbox" value="C.Varken bra eller dåligt" name="answer[]"><label>C.Varken bra eller dåligt</label></li>
        <li><input type="checkbox" value="D.Ganska dåligt" name="answer[]"><label>D.Ganska dåligt</label></li>
        <li><input type="checkbox" value="E.Mycket dåligt" name="answer[]"><label>E.Mycket dåligt</label></li>
    </ul>
      <ul>
        <li><label>3.Vad tycker du är bra med att bo i Lundby stadsdel?</label>
        <input type="text" value="3.Vad tycker du är bra med att bo i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt (Nära stan)" name="answer[]"><label>A.Centralt (Nära stan)</label></li>
        <li><input type="checkbox" value="B.Grönt" name="answer[]"><label>B.Grönt</label></li>
        <li><input type="checkbox" value="C.Lugnt" name="answer[]"><label>C.Lugnt</label></li>
        <li><input type="checkbox" value="D.Nära havet (vatten)" name="answer[]"><label>D.Nära havet (vatten)</label></li>
        <li><input type="checkbox" value="E.Bra kommunikationer" name="answer[]"><label>E.Bra kommunikationer</label></li>
        <li><input type="checkbox" value="F.Nära köpcentra" name="answer[]"><label>F.Nära köpcentra</label></li>
        <li><input type="checkbox" value="G.Nära till allt" name="answer[]"><label>G.Nära till allt</label></li>
      </ul>
      <ul>
        <li><label>4.Finns det något du saknar i Lundby stadsdel?</label>
        <input type="text" value="4.Finns det något du saknar i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="nej" name="answer[]" id="no1"><label>Nej</label></li>
        <li><p>Om ja:</p></li>
        <li><input type="checkbox" value="A.Resturanger" name="answer[]" class="disable1"><label>A.Resturanger</label></li>
        <li><input type="checkbox" value="B.Butiker" name="answer[]" class="disable1"><label>B.Butiker</label></li>
        <li><input type="checkbox" value="C.Nöjesliv/kultur" name="answer[]" class="disable1"><label>C.Nöjesliv/kultur</label></li>
        <li><input type="checkbox" value="D.Grönområden" name="answer[]" class="disable1"><label>D.Grönområden</label></li>
        <li><input type="checkbox" value="E.Kommuniaktioner" name="answer[]" class="disable1"><label>E.Kommuniaktioner</label></li>
        <li><input type="checkbox" value="F.Belysning" name="answer[]" class="disable1"><label>F.Belysning</label></li>
        <li><input type="checkbox" value="G.Cykelbanor" name="answer[]" class="disable1"><label>G.Cykelbanor</label></li>
        <li><input type="checkbox" value="H.Service (post,vård,barnomsorg etc)" name="answer[]" class="disable1"><label>H.Service (post,vård,barnomsorg etc)</label></li>
      </ul>
      <ul>
        <li><label>5.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?</label>
        <input type="text" value="5.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt (Nära stan)" name="answer[]"><label>A.Centralt (Nära stan)</label></li>
        <li><input type="checkbox" value="B.Grönt" name="answer[]"><label>B.Grönt</label></li>
        <li><input type="checkbox" value="C.Lugnt" name="answer[]"><label>C.Lugnt</label></li>
        <li><input type="checkbox" value="D.Marint" name="answer[]"><label>D.Marint</label></li>
        <li><input type="checkbox" value="E.Trafikerat" name="answer[]"><label>E.Trafikerat</label></li>
        <li><input type="checkbox" value="F.Shopping/servicemöljigheter" name="answer[]"><label>F.Shopping/servicemöljigheter</label></li>
      </ul>
      <ul>
        <li><label>6.Känner du dig trygg i Lundby stadsdel?</label>
        <input type="text" value="6.Känner du dig trygg i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Ja" name="answer[]" id="yes"><label>Ja</label></li>
        <li><p>Om nej. Vad beror det på?</p></li>
        <li><input type="checkbox" value="A.Brottslighet" name="answer[]" class="disable2"><label>A.Brottslighet</label></li>
        <li><input type="checkbox" value="B.Bråk" name="answer[]" class="disable2"><label>B.Bråk</label></li>
        <li><input type="checkbox" value="C.Folktomt (ödsligt)" name="answer[]" class="disable2"><label>C.Folktomt (ödsligt)</label></li>
        <li><input type="checkbox" value="D.Få poliser" name="answer[]" class="disable2"><label>D.Få poliser</label></li>
        <li><input type="checkbox" value="E.Mörkt/dåligt upplyst" name="answer[]" class="disable2"><label>E.Mörkt/dåligt upplyst</label></li>
        <li><input type="checkbox" value="F.Inbrott" name="answer[]" class="disable2"><label>F.Inbrott</label></li>
        </ul>
      <ul>
        <li><label>7.Tycker du att Lundby stadel har något speciell styrka?</label>
        <input type="text" value="7.Tycker du att Lundby stadel har något speciell styrka?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Nej" name="answer[]" id="no2"><label>Nej</label></li>
        <li><p>Om ja, vilken/vilka styrkor?</p></li>
        <li><input type="checkbox" value="A.Centralt (nära stan)" name="answer[]" class="disable3"><label>A.Centralt (nära stan)</label></li>
        <li><input type="checkbox" value="B.Här händer mycket (utveckling,boende,jobb)" name="answer[]" class="disable3"><label>B.Här händer mycket (utveckling,boende,jobb)</label></li>
        <li><input type="checkbox" value="C.Grönt" name="answer[]" class="disable3"><label>C.Grönt</label></li>
        <li><input type="checkbox" value="D.Lugnt" name="answer[]" class="disable3"><label>D.Lugnt</label></li>
        <li><input type="checkbox" value="E.Bra bostadspriser" name="answer[]" class="disable3"><label>E.Bra bostadspriser</label></li>
        <li><input type="checkbox" value="F.Bra föreningsmöljigheter" name="answer[]" class="disable3"><label>F.Bra föreningsmöljigheter</label></li>
       </ul>
      <ul>
        <li><label>8.Tycker du att Lundby stadsdel har någon speciell svaghet?</label>
        <input type="text" value="8.Tycker du att Lundby stadsdel har någon speciell svaghet?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Nej" name="answer[]" id="no3"><label>Nej</label></li>
        <li><p>Om ja, vilken/vilka svagheter?</p></li>
        <li><input type="checkbox" value="A.Alla vägar/leder och trafik" name="answer[]" class="disable4"><label>A.Alla vägar/leder och trafik</label></li>
        <li><input type="checkbox" value="B.Brottslighet/otryggt" name="answer[]" class="disable4"><label>B.Brottslighet/otryggt</label></li>
        <li><input type="checkbox" value="C.Nöjesliv/kultur" name="answer[]" class="disable4"><label>C.Nöjesliv/kultur</label></li>
        <li><input type="checkbox" value="D.Dåliga förbindelser till Gbg centrum" name="answer[]" class="disable4"><label>D.Dåliga förbindelser till Gbg centrum</label></li>
       </ul>
      <ul>
        <li><label>9.Hur skulle du vilja att Lundby uppfattades om 5år?</label>
        <input type="text" value="9.Hur skulle du vilja att Lundby uppfattades om 5år?" style="display:none" name="question[]"></li>
        <li><p>Som</p>
        <li><input type="checkbox" value="Den gröna stadsdelen i Göteborg" name="answer[]"><label>*Den gröna stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den rockiga stadsdelen i Göteborg" name="answer[]"><label>*Den rockiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lyxiga stadsdelen i Göteborg" name="answer[]"><label>*Den lyxiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den hippa stadsdelen i Göteborg" name="answer[]"><label>*Den hippa stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lantiga stadsdelen i Göteborg" name="answer[]"><label>*Den lantiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lugna stadsdelen i Göteborg" name="answer[]"><label>*Den lugna stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den alternativa stadsdelen i Göteborg" name="answer[]"><label>*Den alternativa stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den mångkulturella stadsdelen i Göteborg" name="answer[]"><label>*Den mångkulturella stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den framtida stadsdelen i Göteborg" name="answer[]"><label>*Den framtida stadsdelen i Göteborg</label></li>
       </ul>
      <ul>
        <li><label>10.Vilket område förknippad du med stadsdelen Lundby?</label>
        <input type="text" value="10.Vilket område förknippad du med stadsdelen Lundby?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Backaplan" name="answer[]"><label>A.Backaplan</label></li>
        <li><input type="checkbox" value="B.Wieselgrenplatsen" name="answer[]"><label>B.Wieselgrenplatsen</label></li>
        <li><input type="checkbox" value="C.Kvilletorget" name="answer[]"><label>C.Kvilletorget</label></li>
        <li><input type="checkbox" value="D.Norra Älvstranden" name="answer[]"><label>D.Norra Älvstranden</label></li>
        <li><input type="checkbox" value="E.Lundbybadet/Rambergsvallen" name="answer[]"><label>E.Lundbybadet/Rambergsvallen</label></li>
        <li><input type="checkbox" value="F.Kärradalen" name="answer[]"><label>F.Kärradalen</label></li>
        <li><input type="checkbox" value="G.Kyrkbyn" name="answer[]"><label>G.Kyrkbyn</label></li>
        </ul>
      <ul>
        <li><label>11.Var i Lundby bor du?</label>
        <input type="text" value="11.Var i Lundby bor du?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Kvillebäcken" name="answer[]"><label>A.Kvillebäcken</label></li>
        <li><input type="checkbox" value="B.Slättadamm" name="answer[]"><label>B.Slättadamm</label></li>
        <li><input type="checkbox" value="C.Kärrdalen" name="answer[]"><label>C.Kärrdalen</label></li>
        <li><input type="checkbox" value="D.Kyrkbyn" name="answer[]"><label>D.Kyrkbyn</label></li>
        <li><input type="checkbox" value="E.Rambergstaden" name="answer[]"><label>E.Rambergstaden</label></li>
        <li><input type="checkbox" value="F.Eriksberg" name="answer[]"><label>F.Eriksberg</label></li>
        <li><input type="checkbox" value="G.Lindholmen" name="answer[]"><label>G.Lindholmen</label></li>
      </ul>
      <div id="gender">
        <input type="radio" name="gender" id="male" value="male"><label for="male">Man</label>
        <input type="radio" name="gender" id="female" value="female"><label for="female">Kvinna</label>
      </div>
      <br>
      <div id="age">
        <label>Ålder:</label>
        <input type="radio" name="age" value="10-20"><label for="age">10-20</label>
        <input type="radio" name="age" value="21-30"><label for="age">21-30</label>
        <input type="radio" name="age" value="31-40"><label for="age">31-40</label>
        <input type="radio" name="age" value="41-50"><label for="age">41-50</label>
        <input type="radio" name="age" value="51-65"><label for="age">51-65</label>
        <input type="radio" name="age" value="66+"><label for="age">66+</label>
      </div>
      <input type="submit" value="skicka" id="submitButton">
    </form>
    </div>
  </div>
     <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- JQUERY UI -->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <!-- LESS -->
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/checked.js"></script>
    <script type="text/javascript" src="js/ajaxInput.js"></script>
    </body>
    </html>