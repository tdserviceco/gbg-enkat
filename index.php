<!DOCTYPE html>
<!--
THIS SITE IS OPTIMISED FOR TABLETS ONLY!!! MAINLY IPADS!
THE CLIENT WANTED IT USED FOR IPAD MINI OR IPAD 2/3/4/AIR
-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    </head>
    <body>
    <header>&nbsp;</header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
      <div id="areYou">
        <ul>
            <li>
                <a href="privat/privat.php" target="_self">PRIVAT</a>
            </li>
            <li>
                <a href="foretag/foretag.php" target="_self">FÖRETAGARE</a>
            </li>
            <li>
                <a href="orgination/org.php" target="_self">ORGANISATION</a>
            </li>
             <li>
                <a href="stadsbor/stadsbor.php" target="_self">STADSBOR</a>
            </li>
        </ul>
      </div>  
  </div>
     <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- JQUERY UI -->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <!-- LESS -->
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function() {
    $( "a" )
      .button()
      .click(function() {
      });
     });
    </script>
    </body>
    </html>