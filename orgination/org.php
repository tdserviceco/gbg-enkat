<!DOCTYPE html>
<!--
THIS SITE IS OPTIMISED FOR TABLETS ONLY!!! MAINLY IPADS!
THE CLIENT WANTED IT USED FOR IPAD MINI OR IPAD 2/3/4/AIR
-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header><div id="linkToResult"><a href="result.php" target="_self">Resultat sidan</a></header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
    <div id="questions">
      <form id="organisationForm">
      <ul>
      <li>
        <label>1.Hur trivs du att arbeta Lundby stadsdel?</label>
        <input type="text" value="1.Hur trivs du att arbeta Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Mycket bra" name="answer[]"><label>A.Mycket bra</label></li>
        <li><input type="checkbox" value="B.Ganska bra" name="answer[]"><label>B.Ganska bra</label></li>
        <li><input type="checkbox" value="C.Varken bra eller dåligt" name="answer[]"><label>C.Varken bra eller dåligt</label></li>
        <li><input type="checkbox" value="D.Ganska dåligt" name="answer[]"><label>D.Ganska dåligt</label></li>
        <li><input type="checkbox" value="E.Mycket dåligt" name="answer[]"><label>E.Mycket dåligt</label></li>
      </ul>
      <ul>
        <li><label>2.Hur tycker du det är att bo i Lundby stadsdel?</label>
        <input type="text" value="2.Hur tycker du det är att bo i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt (nära stan,stadskänsla)" name="answer[]"><label>A.Centralt (nära stan,stadskänsla)</label></li>
        <li><input type="checkbox" value="B.Grönt" name="answer[]"><label>B.Grönt</label></li>
        <li><input type="checkbox" value="C.Lugnt" name="answer[]"><label>C.Lugnt</label></li>
        <li><input type="checkbox" value="D.Vattennära" name="answer[]"><label>D.Vattennära</label></li>
        <li><input type="checkbox" value="E.Trafikerat" name="answer[]"><label>E.Trafikerat</label></li>
        <li><input type="checkbox" value="F.Shopping/servicemöljigheter" name="answer[]"><label>F.Shopping/servicemöljigheter</label></li>
      </ul>
      <ul>
        <li><label>3.Vilka fördel tycker du Lundby stadsdel har?</label>
        <input type="text" value="3.Vilka fördel tycker du Lundby stadsdel har?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt (Nära stan)" name="answer[]"><label>A.Centralt (Nära stan)</label></li>
        <li><input type="checkbox" value="B.Fina grönområde" name="answer[]"><label>B.Fina grönområde</label></li>
        <li><input type="checkbox" value="C.Lugnt" name="answer[]"><label>C.Lugnt</label></li>
        <li><input type="checkbox" value="D.Vattennära" name="answer[]"><label>D.Vattennära</label></li>
        <li><input type="checkbox" value="E.Bra kommunikationer" name="answer[]"><label>E.Bra kommunikationer</label></li>
        <li><input type="checkbox" value="F.Bra shopping/servicemöljigheter" name="answer[]"><label>F.Bra shopping/servicemöljigheter</label></li>
        <li><input type="checkbox" value="G.Bra cyckelvägar" name="answer[]"><label>G.Bra cyckelvägar</label></li>
      </ul>
      <ul>
        <li><label>4.Saknar du något i stadsdelen?</label>
        <input type="text" value="4.Saknar du något i stadsdelen?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Nej" name="answer[]" id="no"><label>Nej</label></li>
        <li><p>Om ja:</p></li>
        <li><input type="checkbox" value="A.Bättre service(Sjukvård,apotek etc)" name="answer[]" class="disable"><label>A.Bättre service(Sjukvård,apotek etc)</label></li>
        <li><input type="checkbox" value="B.Resturanger/matställen" name="answer[]" class="disable"><label>B.Resturanger/matställen</label></li>
        <li><input type="checkbox" value="C.Bättre cykelvägar" name="answer[]" class="disable"><label>C.Bättre cykelvägar</label></li>
        <li><input type="checkbox" value="D.Utbudet av kvällsaktiviteter" name="answer[]" class="disable"><label>D.Utbudet av kvällsaktiviteter</label></li>
        <li><input type="checkbox" value="E.Fler kunder" name="answer[]" class="disable"><label>E.Fler kunder</label></li>
      </ul>
      <input type="submit" value="skicka" id="submitButton">
    </form>
    </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/ajaxInput.js"></script>
    <script type="text/javascript">
    $('#no').change(function(){
    if ($('#no').is(':checked') == true){
      $('.disable').prop('disabled', true);
    }
    else{
    $('.disable').prop('disabled', false);
    }
    });
   </script>
    </body>
    </html>