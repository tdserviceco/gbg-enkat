jQuery(function($) {
    var answers = {

        '1.Hur trivs du att arbeta Lundby stadsdel?': ['A.Mycket bra','B.Ganska bra','C.Varken bra eller dåligt','D.Ganska dåligt','E.Mycket dåligt'],
        '2.Hur tycker du det är att bo i Lundby stadsdel?': ['A.Centralt (nära stan, stadskänsla)','B.Fina grönområde','C.Lugnt','D.Vattennära','E.Trafikerat','F.Shopping/servicemöljighete'],
        '3.Vilka fördel tycker du Lundby stadsdel har?': ['A.Centralt (nära stan, stadskänsla)','B.Fina grönområde','C.Lugnt','D.Vattennära','E.Bra kommunikationer','F.Bra shopping/servicemöljigheter','G.Bra cyckelvägar'],
        '4.Saknar du något i stadsdelen?': ['Nej','--OM NEJ--','A.Bättre service(Sjukvård,apotek etc)','B.Resturanger/matställen','C.Bättre cykelvägar','D.Utbudet av kvällsaktiviteter','E.Fler kunder'],
    }
    
    var $answers = $('#answer');
    $('#question').change(function () {
        var question = $(this).val(), lcns = answers[question] || [];
        
        var html = $.map(lcns, function(lcn){
            return '<option value="' + lcn + '" name="answer">' + lcn + '</option>'
        }).join('');
        $answers.html(html)
    });
});
