<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header>&nbsp;</header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
        <form id="result">
            <select id="question" name="question">
                <option></option>
                <option value="1.Vad är fördelen med att bedriva verksamhet i Lundby stadsdel?" name="question">1.Vad är fördelen med att bedriva verksamhet i Lundby stadsdel?
                </option>
                <option value="2.Hur tycker du det är att bo i Lundby stadsdel?" name="question">2.Hur tycker du det är att bo i Lundby stadsdel?</option>
                <option value="3.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?" name="question">3.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?</option>
                <option value="4.Vad saknar du i stadsdelen?" name="question">4.Vad saknar du i stadsdelen?</option>
                <option value="5.Är verksamheten kvar här om 5år?" name="question">5.Är verksamheten kvar här om 5år?</option>
                <option value="6.Vad tycker du skulle kunna locka fler företag till stadsdelen?" name="question">6.Vad tycker du skulle kunna locka fler företag till stadsdelen?
                </option>
                <option value="7.Hur skulle du vilja att Lundby uppfattades om 5år?" name="question">7.Hur skulle du vilja att Lundby uppfattades om 5år?</option>
                <option value="8.Var i Lundby stadsdel har ni er verksamhet?" name="question">8.Var i Lundby stadsdel har ni er verksamhet?</option>
            </select>
            <br />
            <br />
            <select id="answer" name="answer"></select>
            <input type="submit" name="submit" id="submit" value="Skicka">
        </form>
        <br />
        <ul class="chart">
        </ul>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/result.js"></script>
    <script type="text/javascript" src="js/selector.js"></script>
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    </body>
</html>
