jQuery(function($) {
    var answers = {

        '1.Vad är fördelen med att bedriva verksamhet i Lundby stadsdel?': ['A.Centralt','B.Vattennära miljö','C.Område med framtidskänsla','D.Representativt','E.Lättåterkomligt, bra infrastruktur'],
        '2.Hur tycker du det är att bo i Lundby stadsdel?': ['A.Mycket bra','B.Ganska bra','C.Varken bra eller dåligt','D.Ganska dåligt','E.Mycket dåligt'],
        '3.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?': ['A.Centralt (nära stan, stadskänsla)','B.Grönt','C.Lugnt','D.Vattennära','E.Trafikerat','F.Shopping/servicemöljigheter'],
        '4.Vad saknar du i stadsdelen?': ['Inget','--OM INGET--','A.Bättre kommunikatioener','B.Fler älvförbindelser','C.Information om arbetskraft','D.Bättre kontakt med stadsdelsförvaltningen','E.Ökat nätverkande bland företag'],
        '5.Är verksamheten kvar här om 5år?': ['Ja','Nej','Vet ej'],
        '6.Vad tycker du skulle kunna locka fler företag till stadsdelen?': ['A.Fler förbindelser över/under älven','B.Bättre service','C.Fler bostäder','D.Marknadsföring'],
        '7.Hur skulle du vilja att Lundby uppfattades om 5år?': ['Den gröna stadsdelen i Göteborg','Den rockiga stadsdelen i Göteborg','Den lyxiga stadsdelen i Göteborg','Den hippa stadsdelen i Göteborg','Den lantiga stadsdelen i Göteborg','Den lugna stadsdelen i Göteborg','Den alternativa stadsdelen i Göteborg','Den mångkulturella stadsdelen i Göteborg','Den framtida stadsdelen i Göteborg'],
        '8.Var i Lundby stadsdel har ni er verksamhet?': ['A.Slättadamm','B.Kärrdalen','C.Kyrkbyn','D.Rambergstaden','E.Eriksberg','F.Lindholmen'],
        
    
    }
    
    var $answers = $('#answer');
    $('#question').change(function () {
        var question = $(this).val(), lcns = answers[question] || [];
        
        var html = $.map(lcns, function(lcn){
            return '<option value="' + lcn + '" name="answer">' + lcn + '</option>'
        }).join('');
        $answers.html(html)
    });
});
