<!DOCTYPE html>
<!--
THIS SITE IS OPTIMISED FOR TABLETS ONLY!!! MAINLY IPADS!
THE CLIENT WANTED IT USED FOR IPAD MINI OR IPAD 2/3/4/AIR
-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header><div id="linkToResult"><a href="result.php" target="_self">Resultat sidan</a></header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
    <div id="questions">
      <form id="businessForm">
      <ul>
      <li>
        <label>1.Vad är fördelen med att bedriva verksamhet i Lundby stadsdel?</label>
        <input type="text" value="1.Vad är fördelen med att bedriva verksamhet i Lundby stadsdel?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt" name="answer[]"><label>A.Centralt</label></li>
        <li><input type="checkbox" value="B.Vattennära miljö" name="answer[]"><label>B.Vattennära miljö</label></li>
        <li><input type="checkbox" value="C.Område med framtidskänsla" name="answer[]"><label>C.Område med framtidskänsla</label></li>
        <li><input type="checkbox" value="D.Representativt" name="answer[]"><label>D.Representativt</label></li>
        <li><input type="checkbox" value="E.Lättåterkomligt, bra infrastruktur" name="answer[]"><label>E.Lättåterkomligt, bra infrastruktur</label></li>
      </ul>
      <ul>
        <li><label>2.Hur tycker du det är att bo i Lundby stadsdel?</label></li>
        <input type="text" value="2.Hur tycker du det är att bo i Lundby stadsdel?" style="display:none" name="question[]">
        <li><input type="checkbox" value="A.Mycket bra" name="answer[]"><label>A.Mycket bra</label></li>
        <li><input type="checkbox" value="B.Ganska bra" name="answer[]"><label>B.Ganska bra</label></li>
        <li><input type="checkbox" value="C.Varken bra eller dåligt" name="answer[]"><label>C.Varken bra eller dåligt</label></li>
        <li><input type="checkbox" value="D.Ganska dåligt" name="answer[]"><label>D.Ganska dåligt</label></li>
        <li><input type="checkbox" value="E.Mycket dåligt" name="answer[]"><label>E.Mycket dåligt</label></li>
      </ul>
      <ul>
        <li><label>3.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?</label>
        <input type="text" value="3.Hur skulle du beskriva Lundby stadsdel för någon som inte kände till den?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Centralt (nära stan, stadskänsla)" name="answer[]"><label>A.Centralt (nära stan, stadskänsla)</label></li>
        <li><input type="checkbox" value="B.Grönt" name="answer[]"><label>B.Grönt</label></li>
        <li><input type="checkbox" value="C.Lugnt" name="answer[]"><label>C.Lugnt</label></li>
        <li><input type="checkbox" value="D.Vattennära" name="answer[]"><label>D.Vattennära</label></li>
        <li><input type="checkbox" value="E.Trafikerat" name="answer[]"><label>E.Trafikerat</label></li>
        <li><input type="checkbox" value="F.Shopping/servicemöljigheter" name="answer[]"><label>F.Shopping/servicemöljigheter</label></li>
      </ul>
      <ul>
        <li><label>4.Vad saknar du i stadsdelen?</label></li>
        <input type="text" value="4.Vad saknar du i stadsdelen?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Inget" name="answer[]" id="no"><label>Inget</label></li>
        <li><p>Om inget</p></li>
        <li><input type="checkbox" value="A.Bättre kommunikatioener" name="answer[]" class="disable"><label>A.Bättre kommunikatioener</label></li>
        <li><input type="checkbox" value="B.Fler älvförbindelser" name="answer[]" class="disable"><label>B.Fler älvförbindelser</label></li>
        <li><input type="checkbox" value="C.Information om arbetskraft" name="answer[]" class="disable"><label>C.Information om arbetskraft</label></li>
        <li><input type="checkbox" value="D.Bättre kontakt med stadsdelsförvaltningen" name="answer[]" class="disable"><label>D.Bättre kontakt med stadsdelsförvaltningen</label></li>
        <li><input type="checkbox" value="E.Ökat nätverkande bland företag" name="answer[]" class="disable"><label>E.Ökat nätverkande bland företag</label></li>
        </ul>
      <ul>
        <li><label>5.Är verksamheten kvar här om 5år?</label>
        <input type="text" value="5.Är verksamheten kvar här om 5år?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="Ja" name="answer[]"><label>Ja</label></li>
        <li><input type="checkbox" value="Nej" name="answer[]"><label>Nej</label></li>
        <li><input type="checkbox" value="Vet ej" name="answer[]"><label>Vet ej</label></li>
       </ul>
      <ul>
        <li><label>6.Vad tycker du skulle kunna locka fler företag till stadsdelen?</label>
        <input type="text" value="6.Vad tycker du skulle kunna locka fler företag till stadsdelen?" style="display:none" name="question[]"></li>
        <li><input type="checkbox" value="A.Fler förbindelser över/under älven" name="answer[]"><label>A.Fler förbindelser över/under älven</label></li>
        <li><input type="checkbox" value="B.Bättre service" name="answer[]"><label>B.Bättre service</label></li>
        <li><input type="checkbox" value="C.Fler bostäder" name="answer[]"><label>C.Fler bostäder</label></li>
        <li><input type="checkbox" value="D.Marknadsföring" name="answer[]"><label>D.Marknadsföring</label></li>
       </ul>
      <ul>
        <li><label>7.Hur skulle du vilja att Lundby uppfattades om 5år?</label></li>
        <input type="text" value="7.Hur skulle du vilja att Lundby uppfattades om 5år?" style="display:none" name="question[]">
        <li><p>Som</p>
        <li><input type="checkbox" value="Den gröna stadsdelen i Göteborg" name="answer[]"><label>*Den gröna stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den rockiga stadsdelen i Göteborg" name="answer[]"><label>*Den rockiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lyxiga stadsdelen i Göteborg" name="answer[]"><label>*Den lyxiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den hippa stadsdelen i Göteborg" name="answer[]"><label>*Den hippa stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lantiga stadsdelen i Göteborg" name="answer[]"><label>*Den lantiga stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den lugna stadsdelen i Göteborg" name="answer[]"><label>*Den lugna stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den alternativa stadsdelen i Göteborg" name="answer[]"><label>*Den alternativa stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den mångkulturella stadsdelen i Göteborg" name="answer[]"><label>*Den mångkulturella stadsdelen i Göteborg</label></li>
        <li><input type="checkbox" value="Den framtida stadsdelen i Göteborg" name="answer[]"><label>*Den framtida stadsdelen i Göteborg</label></li>
       </ul>
      <ul>
        <li><label>8.Var i Lundby stadsdel har ni er verksamhet?</label></li>
        <input type="text" value="8.Var i Lundby stadsdel har ni er verksamhet?" style="display:none" name="question[]">
        <li><input type="checkbox" value="A.Slättadamm" name="answer[]"><label>A.Slättadamm</label></li>
        <li><input type="checkbox" value="B.Kärrdalen" name="answer[]"><label>B.Kärrdalen</label></li>
        <li><input type="checkbox" value="C.Kyrkbyn" name="answer[]"><label>C.Kyrkbyn</label></li>
        <li><input type="checkbox" value="D.Rambergstaden" name="answer[]"><label>D.Rambergstaden</label></li>
        <li><input type="checkbox" value="E.Eriksberg" name="answer[]"><label>E.Eriksberg</label></li>
        <li><input type="checkbox" value="F.Lindholmen" name="answer[]"><label>F.Lindholmen</label></li>
      </ul>
      <input type="submit" value="skicka" id="submitButton">
    </form>
    </div>
  </div>
     <!-- JQUERY -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <!-- JQUERY UI -->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <!-- LESS -->
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    <script src="js/ajaxInput.js" type="text/javascript"></script>
    <script type="text/javascript">
    $('#no').change(function(){
    if ($('#no').is(':checked') == true){
      $('.disable').prop('disabled', true);
    }
    else{
    $('.disable').prop('disabled', false);
    }
    });
    </script>
    </body>
    </html>