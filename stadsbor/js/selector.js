jQuery(function($) {
    var answers = {

        '1.Vad förknippar du med Lundby Stadsdel?': ['A.Backaplan','B.Norra Älvstranden','C.Hisingen','D.Industrier','E.Vatten/Båtar/Varv'],
        '2.Vad tycker du om Lundby Stadsdel?': ['A.Mycket bra','B.Ganska bra','C.Varken bra eller dåligt','D.Ganska dåligt','E.Mycket dåligt'],
    
    }
    
    var $answers = $('#answer');
    $('#question').change(function () {
        var question = $(this).val(), lcns = answers[question] || [];
        
        var html = $.map(lcns, function(lcn){
            return '<option value="' + lcn + '" name="answer">' + lcn + '</option>'
        }).join('');
        $answers.html(html)
    });
});
