$(document).ready(function() {
	counts();
});
	
function counts(){
	$("#result").submit(function() {
		var data = $("#result").serialize();
		$.post('include/getResult.php',data,function(data)
		{
			$("ul").empty();
			data = JSON.parse(data);
			$.each(data.result, function(){
			var number1 = this['countAnswer'];
			var number2 = 500;
			var result = (number1 / number2) * 100;
			$("ul").append("<li class='axis'><div class='label'>500 st</div><div class='label'>250 st</div><div class='label'>150 st</div><div class='label'>100 st</div><div class='label'>50 st</div></li><li class='bar teal' style='height:"+result+"%;'><div class='percent'>"+this['countAnswer']+"<span>st</span></div><div class='skill'>Antal Svar</div></li>");
		});
		});

		return false;
	});
}
