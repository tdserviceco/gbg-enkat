<!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Attitydundersökning om stadsdelen Lundby</title>
        <meta name="description" content="Attitydundersökning om stadsdelen Lundby">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" type="image/png" href="img/favicon.ico">
        <link rel="apple-touch-icon" sizes="76x76" href="img/touch-icon-ipad.png" /> 
        <link rel="apple-touch-icon" sizes="152x152" href="img/touch-icon-ipad-retina.png" />
        <link rel="stylesheet/less" type="text/css" href="less/lundby.less"/>
        <link rel="stylesheet/less" type="text/css" href="less/index.less" />
    </head>
    <body>
    <header><div id="linkToResult"><a href="result.php" target="_self">Resultat sidan</a></header>
    <div id="bodyContainer">
      <img src="img/lundby_rgb_small.png" alt="Hisingen Lundby">
    <div id="questions">
      <form id="cityForm">
      <ul>
        <li>
            <label>1.Vad förknippar du med Lundby Stadsdel?</label>
            <input type="text" value="1.Vad förknippar du med Lundby Stadsdel?" style="display:none" name="question[]">
        </li>
        <li>
            <input type="checkbox" value="A.Backaplan" name="answer[]">
            <label>A.Backaplan</label>
        </li>
        <li>
            <input type="checkbox" value="B.Norra Älvstranden" name="answer[]">
            <label>B.Norra Älvstranden</label>
        </li>
        <li>
            <input type="checkbox" value="C.Hisingen" name="answer[]">
            <label>C.Hisingen</label>
        </li>
        <li>
            <input type="checkbox" value="D.Industrier" name="answer[]">
            <label>D.Industrier</label>
        </li>
        <li>
            <input type="checkbox" value="E.Vatten/Båtar/Varv" name="answer[]">
            <label>E.Vatten/Båtar/Varv</label>
        </li>
        </ul>
        <ul>
        <li>
            <label>2.Vad tycker du om Lundby Stadsdel?</label>
            <input type="text" value="2.Vad tycker du om Lundby Stadsdel?" style="display:none" name="question[]">
        </li>
        <li>
            <input type="checkbox" value="A.Mycket bra" name="answer[]">
            <label>A.Mycket Bra</label>
        </li>
        <li>
            <input type="checkbox" value="B.Ganska bra" name="answer[]">
            <label>B.Ganska Bra</label>
        </li>
        <li>
            <input type="checkbox" value="C.Varken bra eller dåligt" name="answer[]">
            <label>C.Varken bra eller dåligt</label>
        </li>
        <li>
            <input type="checkbox" value="D.Ganska dåligt" name="answer[]">
            <label>D.Ganska dåligt</label>
        </li>
        <li>
            <input type="checkbox" value="E.Mycket dåligt" name="answer[]">
            <label>E.Mycket dåligt</label>
        </li>
      </ul>
      <input type="submit" value="skicka" name="submit" id="submitButton">
    </form>
    </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/ajaxInput.js"></script>
    <script src="js/less-1.6.0.min.js" type="text/javascript"></script>
    </body>
    </html>